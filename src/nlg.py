#!/usr/local/bin/python

import asyncio
import copy
import datetime
import logging
import os
import random
from string import Formatter
from typing import Any, Dict
from urllib.parse import urljoin
import urllib.parse
from enum import Enum

import aiohttp
from ruamel.yaml import YAML
from sanic import Sanic
from sanic.request import Request
from sanic import response
from sanic.response import HTTPResponse
from slugify import slugify
from tenacity import (
    retry,
    wait_exponential,
    stop_after_attempt,
    retry_if_exception_type,
)

# Setup logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Sentry setup
sentry_dsn = os.getenv("NLG_SENTRY_DSN", None)
if sentry_dsn:
    import sentry_sdk
    from sentry_sdk.integrations.sanic import SanicIntegration
    from sentry_sdk.integrations.redis import RedisIntegration

    sentry_sdk.init(
        dsn=sentry_dsn, integrations=[SanicIntegration(), RedisIntegration()]
    )

# Initialize Sanic app
app = Sanic(name="nlg")
class ProcessingStatus(str, Enum):
    OK = "done"
    IN_PROGRESS = "inprogress"
    ERROR = "error"
    NOT_FOUND = "notfound"


class ProcessingTracker:
    def __init__(self):
        self.status: Dict[str, ProcessingStatus] = {}

    def start_processing(self, etag: str):
        logger.info(f"Starting processing tracker for ETag: {etag}")
        self.status[etag] = ProcessingStatus.IN_PROGRESS

    def complete_processing(self, etag: str):
        logger.info(f"Completing processing tracker for ETag: {etag}")
        self.status[etag] = ProcessingStatus.OK

    def mark_error(self, etag: str):
        logger.error(f"Marking error for ETag: {etag}")
        self.status[etag] = ProcessingStatus.ERROR

    def get_status(self, etag: str) -> ProcessingStatus:
        logger.info(f"Getting status for ETag: {etag}")
        return self.status.get(etag, ProcessingStatus.NOT_FOUND)
    
    def get_all_etag_status(self):
        return self.status


class SlotFormatter:
    def fill_in_slots(self, text: str, slots):
        """
        Based on a textual response, fill in any slots (or arguments) we have received from Rasa.
        Ignore any empty slots.
        """
        if not slots:
            return text
        for key, value in slots.items():
            if key and value:
                if isinstance(value, list):
                    value = value[0]
                if not isinstance(value, str):
                    continue
                text = text.replace("{%s}" % key, value)
        return text


class UtterSelector:
    @staticmethod
    def get_utter_random(utters, channel):
        default_utters = [u for u in utters if not "channel" in u]
        usable_utters = default_utters

        if channel:
            specific_utters = [u for u in utters if u.get("channel", "") == channel]
            if specific_utters:
                usable_utters = specific_utters

        utter = random.choice(usable_utters)
        return copy.deepcopy(utter)


class LiveChatService:
    @staticmethod
    async def livechat_available(
        organisation=None,
        chat_started_on_page=None,
        environment_url=None,
        feature_livechat=None,
        **kwargs,
    ):
        if feature_livechat:
            return feature_livechat == "open"

        json = {"organisation": organisation, "url": chat_started_on_page}
        headers = {"Content-Type": "application/json"}
        logger.info(
            "Checking livechat status for: %s, %s, %s", json, headers, environment_url
        )
        try:
            async with aiohttp.ClientSession() as client_session:
                async with client_session.post(
                    f"{environment_url}/livechat-status/",
                    json=json,
                    headers=headers,
                    timeout=2,
                ) as resp:
                    status_code = resp.status
                    logger.info(f"Status code for utter retrieval {status_code}")
                    if status_code == 404:
                        logger.info("no livechat connection for %s", organisation)
                        return False
                    availability_response = await resp.json()
                    logger.info(f"Availability: {availability_response}")
                    status = availability_response["status"]
                    return status == "ok"
        except Exception as e:
            logger.error(f"Error occurred while requesting status API: {e}")
            return False


class ButtonFilter:
    CONDITION_MAPPING = {
        "livechat": LiveChatService.livechat_available,
    }

    @classmethod
    async def filter_conditional_buttons(cls, buttons, **kwargs):
        if not buttons:
            return []

        conditions_checked = {}
        filtered_buttons = []
        for button in buttons:
            if button.get("condition"):
                if (
                    "name" not in button["condition"]
                    or "value" not in button["condition"]
                ):
                    logger.error(
                        "Conditional button is missing attributes: %s", buttons
                    )
                    filtered_buttons.append(button)
                    continue

                condition_name = button["condition"]["name"]
                if condition_name not in cls.CONDITION_MAPPING:
                    logger.error(
                        "Unknown button conditions is missing attributes: %s", buttons
                    )
                    filtered_buttons.append(button)
                    continue

                expected_condition_value = button["condition"]["value"]
                if condition_name in conditions_checked:
                    if expected_condition_value == conditions_checked[condition_name]:
                        filtered_buttons.append(button)
                else:
                    condition_value = await cls.CONDITION_MAPPING[condition_name](
                        **kwargs
                    )
                    conditions_checked[condition_name] = condition_value
                    if expected_condition_value == condition_value:
                        filtered_buttons.append(button)
            else:
                filtered_buttons.append(button)

        return filtered_buttons


class NLGResponseBuilder:
    def __init__(self):
        self.slot_formatter = SlotFormatter()

    def construct_nlg_response(
        self,
        utter_name: str,
        central_utter: dict,
        gemeente: str = None,
        slots: list = None,
        arguments: list = None,
        entities: list = None,
        local_response: dict = None,
    ):
        buttons = central_utter.get("buttons", [])
        end2end_buttons = [
            {"title": b["payload"], "payload": b["payload"]} for b in buttons
        ]

        text = local_response["text"] if local_response else central_utter["text"]
        image = (
            local_response["image"]
            if local_response
            else central_utter.get("image", None)
        )

        format_vars = [fn for _, fn, _, _ in Formatter().parse(text) if fn is not None]
        format_dict = {}

        try:
            for var in format_vars:
                format_dict[var] = slots.get(var, arguments.get(var))
        except Exception as e:
            logger.error(f"Error occurred when trying to retrieve slots: {e}")

        context = {}
        for slot, value in central_utter.get("custom", {}).get("context", {}).items():
            if isinstance(value, str):
                try:
                    context[slot] = value.format(**{**slots, **entities, **arguments})
                except KeyError:
                    context[slot] = value
            else:
                context[slot] = value

        statistics = {}
        for slot, value in (
            central_utter.get("custom", {}).get("statistics", {}).items()
        ):
            if isinstance(value, str):
                try:
                    statistics[slot] = value.format(
                        **{**slots, **entities, **arguments}
                    )
                except KeyError:
                    statistics[slot] = value
            else:
                statistics[slot] = value

        response = {
            "text": self.slot_formatter.fill_in_slots(
                self.slot_formatter.fill_in_slots(text, arguments), slots
            ),
            "buttons": buttons,
            "image": image,
            "elements": [],
            "attachments": [],
            "end2end": {
                "template_name": utter_name,
                "template_vars": format_dict,
                "buttons": end2end_buttons,
            },
            "custom": {
                "context": context,
                "statistics": statistics,
                "styling": central_utter.get("custom", {}).get("styling", {}),
            },
        }
        return response


class MetadataExtractor:
    @staticmethod
    def parse_request_data(
        request: Request,
    ) -> (str, Dict[str, Any], Dict[str, str], Dict[str, Any]):
        utter_name = request.json["template"]
        slots = request.json["tracker"]["slots"]
        entities = {
            entity["entity"]: entity["value"]
            for entity in request.json["tracker"]["latest_message"]["entities"]
        }
        arguments = request.json["arguments"]
        return utter_name, slots, entities, arguments

    @staticmethod
    def extract_user_metadata(events: list) -> Dict[str, Any]:
        user_event_metadata = {}
        for event in events:
            if event["event"] == "user" and event.get("metadata"):
                if "municipality" in event["metadata"]:
                    user_event_metadata = event["metadata"]
                    break
        return user_event_metadata

    @staticmethod
    def determine_municipality(slots: dict, events: list) -> str:
        result = [
            event["metadata"].get("municipality")
            for event in events
            if event.get("metadata") and not isinstance(event["metadata"], list)
        ]
        municipality = result[0].lower()
        if slots.get("municipality") is None:
            slots["municipality"] = municipality
        return municipality


class ChannelDeterminer:
    @staticmethod
    def determine_channel(request_data: dict) -> str:
        channel = request_data.get("channel", {}).get("name")
        if channel == "router":
            channel = "socketio"

        if channel == "test":
            channel = "socketio"

        return channel


class ResponsesManager:
    responses_manager = None
    MAX_VERSIONS = 5

    def __init__(self):
        self._responses_by_version = {
            "default": {
                "initialized": False,
                "fingerprint": "0",
                "timestamp": datetime.datetime.min,
                "last_activity": None
            }
        }

    def get_responses(self, version="default"):
        """Retrieve responses for a specific version or return a default dictionary if missing."""
        return self._responses_by_version.setdefault(
            version,
            {
                "initialized": False,
                "fingerprint": "0",
                "timestamp": datetime.datetime.min,
                "last_activity": None
            },
        )

    def set_responses(self, responses, version="default"):
        """Set responses for a specific version, ensuring structure integrity."""
        if responses:
            existing_responses = self._responses_by_version.get(version, {})
            existing_responses.update(responses)
            self._responses_by_version[version] = existing_responses
            self._maintain_max_versions()

    @classmethod
    def get_responses_manager(cls):
        if not cls.responses_manager:
            cls.responses_manager = ResponsesManager()
        return cls.responses_manager

    @staticmethod
    def prepare_responses(responses: dict, version="default"):
        """
        Prepare the responses by adding default values for specific utterances.

        :param responses: A dictionary containing response data.
        :param version: Version key as a string.
        """
        for utter_name, utters in responses.get("responses", {}).items():
            if utter_name.startswith("utter_lo_"):
                # Assume UtterSelector.get_utter_random fetches an utterance's metadata for a given version
                mdutter = UtterSelector.get_utter_random(utters, "metadata")
                # logger.info(f"mdutter for version {version}: {mdutter}")
                if mdutter:
                    default = mdutter["custom"].get("default", [{"text": None}])[0][
                        "text"
                    ]
                    if default:
                        for u in utters:
                            if "channel" not in u:
                                u["text"] = default

    def _maintain_max_versions(self):
        """Trim the _responses_by_version dictionary to only keep the latest 5."""
        # Sort the versions by last_activity, falling back to timestamp
        sorted_versions = sorted(
            self._responses_by_version.items(),
            key=lambda item: (
                item[1].get("last_activity") or item[1]["timestamp"] or datetime.datetime.min
            ),
            reverse=True,
        )
        
        # Keep only the top 5 versions
        self._responses_by_version = dict(sorted_versions[:self.MAX_VERSIONS])

async def fetch_local_response(
    utter_name: str, municipality: str, channel: str
) -> Dict[str, Any]:
    """Fetch local response from the utter API if applicable."""
    if not utter_name.startswith("utter_lo_"):
        return None

    utter_api = os.getenv("UTTER_API_URL")
    if not utter_api:
        return None

    url = f"{utter_api}/api/{slugify(municipality)}/utters/{utter_name}/"
    query_params = {"channel": channel} if channel else {}
    if query_params:
        url = f"{url}?{urllib.parse.urlencode(query_params)}"

    try:
        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(url, timeout=10) as resp:
                if resp.status == 200:
                    local_response = await resp.json()
                    logger.info(
                        f"Response from {municipality} utter api: {local_response}"
                    )
                    return local_response
    except Exception as e:
        logger.error(f"Request to {municipality} utter api failed: {e}")
    return None

# Define the retry logic for checking if the media container is up
@retry(
    retry=retry_if_exception_type(aiohttp.ClientError),
    wait=wait_exponential(multiplier=1, min=4, max=60),
    stop=stop_after_attempt(5),
)
async def get_config_from_media_container(root="gem"):
    """
    Attempts to retrieve configuration from the media container.
    Retries on failure with exponential backoff, up to 5 attempts.
    """
    media_container_url = os.getenv("MEDIA_CONTAINER_URL", "http://media:80")
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{media_container_url}/{root}") as response:
            if response.status == 200:
                return await response.json()
            else:
                raise aiohttp.ClientError(
                    f"Received unexpected status code: {response.status}"
                )

def get_file_paths(root, environment):
    """
    Generate file paths for domain and responses files.
    """
    responses_file_path = f"{root}/{environment}/domain/responses.yml"
    domain_file_path = f"{root}/{environment}/domain"
    return responses_file_path, domain_file_path

async def fetch_response(client_session, url, timeout=20):
    """
    Fetch response from the given URL with a specified timeout.
    """
    return await client_session.get(url, timeout=timeout)

def handle_etag_match(c_responses, version):
    """
    Handle the case where the ETag matches the current fingerprint.
    """
    logger.info(f"Etag matches the current fingerprint ({c_responses['fingerprint']}), skipping update.")
    logger.info(f"Updating last activity for version: {version}")
    c_responses["last_activity"] = datetime.datetime.now()

def update_responses(new_responses, version, etag):
    """
    Update responses with the new data and set relevant metadata.
    """
    new_responses.update(
        {
            "fingerprint": etag,
            "initialized": True,
            "timestamp": datetime.datetime.now(),
        }
    )
    ResponsesManager.get_responses_manager().set_responses(new_responses, version=version)

async def process_new_responses(
    client_session,
    media_url,
    responses_file_path,
    version,
    etag,
    retry_delay=5
):
    """
    Process and update new responses when a new version is detected.
    Keeps retrying indefinitely with a fixed sleep until it succeeds or the task is cancelled.
    """
    processing_tracker.start_processing(etag)
    yaml = YAML(typ="safe")

    while True:
        try:
            async with client_session.get(urljoin(media_url, responses_file_path), timeout=30) as new_resp:
                c_responses_text = await new_resp.text()
                new_responses = yaml.load(c_responses_text)

                update_responses(new_responses, version, etag)

            processing_tracker.complete_processing(etag)
            logger.info(f"Successfully updated responses for version '{version}'")
            return  # Exit the loop once we've succeeded

        except asyncio.CancelledError:
            # If the task is cancelled, respect that and exit immediately
            logger.warning("process_new_responses task was cancelled. Stopping retries.")
            raise

        except Exception as ex:
            logger.error(
                f"Error processing new responses for version '{version}', ETag '{etag}': {ex}. "
                f"Retrying in {retry_delay} seconds..."
            )
            await asyncio.sleep(retry_delay)

# Monitoring task that periodically syncs the project version's responses
async def start_monitoring_version(root, environment, sleep_interval=10):
    """
    Start a monitoring task for a specific project version to sync responses every 10 seconds.
    Adds a new version to the response manager every time a new version is detected.
    """
    logger.info(f"Starting response sync for tenant '{environment}' in project '{root}'")

    media_url = os.getenv("MEDIA_URL", "http://media:80")
    responses_file_path, domain_file_path = get_file_paths(root, environment)
    etag_cache = {}

    async with aiohttp.ClientSession() as client_session:
        while True:
            try:
                headers = {"If-None-Match": etag_cache.get(environment, "")} if etag_cache.get(environment) else {}
                resp = await client_session.get(urljoin(media_url, domain_file_path), headers=headers, timeout=20)
                async with resp:
                    status = resp.status
                    logger.info("Status code for domain file: %s", status)

                    if status == 304:  # No updates
                        logger.info(
                            f"Status code 304. No changes detected in domain file for environment: {environment} (ETag remains the same)."
                        )
                        await asyncio.sleep(sleep_interval)
                        continue

                    if status == 200:
                        etag = resp.headers.get("ETag", "").strip('"')
                        version = etag

                        c_responses = (
                            ResponsesManager.get_responses_manager().get_responses(version=version)
                        )

                        if c_responses.get("initialized") and etag == c_responses.get("fingerprint"):
                            handle_etag_match(c_responses, version)
                            await asyncio.sleep(sleep_interval)
                            continue

                        # Store the new ETag
                        etag_cache[environment] = etag

                        await process_new_responses(client_session, media_url, responses_file_path, version, etag)
                    else:
                        logger.warning(
                            f"Failed to fetch responses for environment: {environment} with status: {status}"
                        )

            except aiohttp.ClientError as client_error:
                logger.error(f"HTTP client error occurred for environment {environment}: {client_error}")
            except asyncio.TimeoutError:
                logger.error(f"Timeout error while syncing responses for environment {environment}.")
            except asyncio.CancelledError:
                logger.info("Caught CancelledError. Stopping monitoring task.")
                break
            except GeneratorExit:
                logger.info("Caught GeneratorExit. Exiting task.")
                break
            except Exception as e:
                logger.error(f"Unexpected error in syncing responses for environment {environment}: {e}")

            await asyncio.sleep(sleep_interval)

# Initialize ProcessingTracker instance
processing_tracker = ProcessingTracker()

# Add health endpoint
@app.route("/health", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    return response.json({"status": "ok"})

# Sanic Routes
@app.route("/", methods=["GET"])
async def root(_: Request) -> HTTPResponse:
    print(f"Current ETags in processing tracker: {processing_tracker.get_all_etag_status()}")
    get_all_etag_status = processing_tracker.get_all_etag_status()
    return response.json({"status": "ok", "etags": str(get_all_etag_status)})

@app.route("/status/<etag>", methods=["GET"])
async def get_dmn_processing_status(_: Request, etag: str):
    print(f"Received status request for ETag: {etag}")

    # Log the current processing tracker status
    try:
        status = processing_tracker.get_status(etag)
        logger.info(f"Status retrieved for ETag {etag}: {status}")
    except Exception as e:
        logger.error(f"Error occurred while retrieving status: {e}")
        return response.json({"error": "Internal server error"}, status=500)

    if status is None:
        logger.warning(f"No status found for ETag: {etag}")
        return response.json({"error": "Invalid ETag", "etag": etag}, status=404)

    return response.json({"status": status, "etag": etag})

@app.route("/nlg", methods=["POST"])
async def nlg(request: Request) -> response.HTTPResponse:
    metadata_extractor = MetadataExtractor()
    channel_determiner = ChannelDeterminer()
    nlg_builder = NLGResponseBuilder()

    utter_name, slots, entities, arguments = metadata_extractor.parse_request_data(request)
    tracker_events = request.json.get("tracker").get("events")
    
    version = next(
        (event["metadata"]["project_name"]
        for event in reversed(tracker_events)
        if "metadata" in event and "project_name" in event["metadata"]),
        None
    )

    if not version:
        return response.text("No version found for: {version} in metadata", status=400)
    
    logger.info(f"Received request for version: {version}")

    events = request.json["tracker"]["events"]
    user_metadata = metadata_extractor.extract_user_metadata(events)
    municipality = metadata_extractor.determine_municipality(slots, events)
    channel = channel_determiner.determine_channel(request.json)

    manager = ResponsesManager.get_responses_manager()

    responses = manager.get_responses(version=version)

    logger.info(f"Initalized responses for version: {version} and initialized: {responses.get('initialized')}")

    if not responses["initialized"]:
        return response.text("NLG not properly initialized", status=500)

    central_utter = UtterSelector.get_utter_random(responses["responses"][utter_name], channel)

    central_utter["buttons"] = await ButtonFilter.filter_conditional_buttons(
        central_utter.get("buttons", []),
        organisation=municipality,
        chat_started_on_page=user_metadata.get("chatStartedOnPage"),
        environment_url=user_metadata.get("environment_url"),
        feature_livechat=user_metadata.get("feature_livechat"),
    )

    local_response = await fetch_local_response(utter_name, municipality, channel)

    res = response.json(
        nlg_builder.construct_nlg_response(
            utter_name,
            central_utter,
            municipality,
            slots,
            arguments,
            entities,
            local_response,
        )
    )

    # Update last_activity timestamp
    if responses is not None and isinstance(responses, dict):
        responses["last_activity"] = datetime.datetime.now()
        manager.set_responses(responses, version=version)
    else:
        logger.error(f"Failed to update last_activity: Invalid responses structure for version {version}")

    return res

# Start the monitoring task for each version once the media container is available
async def start_listener_for_versions():
    """
    Starts the monitoring task for each version once the media container is available.
    """
    root = "gem"
    try:
        payload = await get_config_from_media_container(root=root)
        paths = payload.get("paths")

        if not paths or not isinstance(paths, dict):
            logger.error("Invalid or missing 'paths' in media container payload.")
            raise ValueError("No valid 'paths' found in the media container payload.")

        environments = list(paths.keys())
        logger.info(f"Environments detected: {environments}")

        # Start monitoring tasks for each environment
        for environment in environments:
            asyncio.create_task(start_monitoring_version(root, environment))
    except Exception as e:
        logger.exception(f"Failed to start version listeners: {e}")
        raise

# Continuous health check until the media container is available
async def health_check_media_container(root="gem"):
    """
    Continuously performs a health check on the media container until it is operational.
    Retries indefinitely, with detailed logging.
    """
    while True:
        try:
            logger.info("Performing health check for the media container...")
            await get_config_from_media_container(root=root)
            break  # Exit the loop once the health check succeeds
        except Exception as e:
            logger.info(f"Health check failed: {e}. Retrying...")
            await asyncio.sleep(5)  # Wait before retrying

# Server lifecycle handlers
@app.listener("after_server_start")
async def server_start(app, loop):
    logger.info("Server started. Initializing version listeners...")
    try:
        # Perform the health check
        logger.info("Starting health check for the media container...")
        await health_check_media_container()
        logger.info("Media container health check passed...")
        # Start the monitoring task for each version
        app.add_task(start_listener_for_versions(), name="version_listener")
    except Exception as e:
        logger.exception(f"Error during server start: {e}")

@app.listener("before_server_stop")
async def server_stop(app, loop):
    logger.info("Server stopping. Cancelling version listener tasks...")
    try:
        await app.cancel_task("version_listener")
    except Exception as e:
        logger.exception(f"Error while stopping tasks: {e}")

if __name__ == "__main__":
    app.run(host="0.0.0.0")
