# Extend the official Rasa Core SDK image
FROM registry.gitlab.com/virtuele-gemeente-assistent/gem/base-api:latest

# Copy requirements file and install dependencies without caching
COPY ./requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt

WORKDIR /var/lib/nlg
COPY ./lib /var/lib/nlg
COPY ./src /usr/local/src/nlg
ENV PYTHONPATH "${PYTHONPATH}:/usr/local/src/nlg"
COPY ./src/nlg.py /usr/local/bin/nlg
RUN chmod +x /usr/local/bin/nlg
CMD [ "nlg"]

# Add health check to verify the application is running
HEALTHCHECK --interval=5s --timeout=5s --start-period=5s --retries=5 \
    CMD curl -f http://localhost:8000/health || exit 1